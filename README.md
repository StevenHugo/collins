# Content  
Collins-en-en package for goldendict, lingoes, etc.

# How to use  
 * Unzip ‘Collins Cobuild Audio Dictionary.dsl.files.zip.001' to folder 'Collins Cobuild Audio Dictionary.dsl.files'  and delete the zip files optionally
 * Zip folder 'Collins Cobuild Audio Dictionary.dsl.files' to 'Collins Cobuild Audio Dictionary.dsl.files.zip' and delete the folder optionally  
 * Add the root directory to the dictionary app, such as goldendict, then rescan  